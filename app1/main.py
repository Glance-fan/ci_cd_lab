#Создать приложение, которое определяет 
#в какой из двух вещественных переменных больше цифр после запятой
import os

def compare_digits(num1, num2):
    #определяем сколько цифр после запятой у числа
    dig_in_num1 = len(str(num1).split('.')[1]) if '.' in str(num1) else 0
    dig_in_num2 = len(str(num2).split('.')[1]) if '.' in str(num2) else 0

    if dig_in_num1 > dig_in_num2:
        return f'{num1} имеет больше цифр после запятой'
    elif dig_in_num1 < dig_in_num2:
        return f'{num2} имеет больше цифр после запятой'
    else:
        return 'Оба числа имеют одинаковое количество цифр после запятой'

if __name__ == '__main__':
    #значение из переменных окружения либо дефолтное
    num1 = float(os.environ.get('NUM1', 0.0))
    num2 = float(os.environ.get('NUM2', 0.0))

    print(compare_digits(num1, num2))