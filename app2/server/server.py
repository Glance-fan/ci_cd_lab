# Вывод виде таблице предмета и даты всех экзаменов
import mysql.connector

db_con = mysql.connector.connect(
    database = "lab_db",
    host = "mysql-service",
    user = "root",
    password = "root"
)

curs = db_con.cursor()
curs.execute("SELECT DISTINCT subject_name, exam_date FROM record_book")
result = curs.fetchall()

for row in result:
    print(f'Предмет {row[0]}, Дата экзамена: {row[1]}')