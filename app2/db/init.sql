CREATE DATABASE IF NOT EXISTS lab_db;

USE lab_db;

CREATE TABLE IF NOT EXISTS student_group
(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(250) NOT NULL,
    last_name VARCHAR(250) NOT NULL,
    birthday DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS record_book
(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    student_id INTEGER NOT NULL,
    subject_name VARCHAR(250) NOT NULL,
    exam_date DATE NOT NULL,
    teacher VARCHAR(250) NOT NULL,
    FOREIGN KEY(student_id) REFERENCES student_group (id) ON DELETE CASCADE
);

INSERT INTO student_group (first_name, last_name, birthday) VALUES
('Rustam', 'Timomeev', '2002-08-13'),
('Lev', 'Bruev', '2002-07-26'),
('Andrey', 'Bahmustkiy', '2002-12-07'),
('Roma', 'Vlasov', '2002-03-21');

INSERT INTO record_book (student_id, subject_name, exam_date, teacher) VALUES
(1, 'Math', '2024-01-10', 'Chernyavskaya Irina Alexandrovna'),
(1, 'Physics', '2024-01-13', 'Ivanov Petr Mikhaylovich'),
(1, 'History', '2024-01-17', 'Nesterova Marina Egorovna'),
(2, 'Math', '2024-01-10', 'Chernyavskaya Irina Alexandrovna'),
(2, 'Literature', '2024-01-21', 'Lapteva Nadeshda Yureevna'),
(2, 'Economy', '2024-01-27', 'Karpov Andrey Mikhaylovich');

